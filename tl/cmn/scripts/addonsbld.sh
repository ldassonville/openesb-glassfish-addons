

###################
##Define Routines##
###################
verify_vars()
{
    echo "STARTING: verifying variables"

    if [ x$SRCROOT = x ]; then
        echo "ERROR: You need to set SRCROOT in Hudson"
        return 1
    fi
    if [ x$SVN_URL = x ]; then
        echo "ERROR: You need to set SVN_URL in Hudson"
        return 1
    fi
    if [ x$SVN_BRANCH = x ]; then
        echo "ERROR: You need to set SVN_BRANCH in Hudson"
        return 1
    fi
    if [ x$SMB_KIT_LOC = x ]; then
        echo "ERROR: You need to set SMB_KIT_LOC in Hudson"
        return 1
    fi
    if [ x$NFS_KIT_LOC = x ]; then
        echo "ERROR: You need to set NFS_KIT_LOC in Hudson"
        return 1
    fi
    if [ x$PUSHKIT_IDENTITY = x ]; then
        echo "ERROR: You need to set PUSHKIT_IDENTITY in Hudson"
        return 1
    fi
    if [ x$PUSHKIT_DEST = x ]; then
        echo "ERROR: You need to set PUSHKIT_DEST in Hudson"
        return 1
    fi
    if [ x$JAVA_HOME = x ]; then
        echo "ERROR: You need to set JAVA_HOME in Hudson"
        return 1
    fi
    if [ x$ANT_HOME = x ]; then
        echo "ERROR: You need to set ANT_HOME in Hudson"
        return 1
    fi
    if [ x$MAVEN_HOME = x ]; then
        echo "ERROR: You need to set MAVEN_HOME in Hudson"
        return 1
    fi
    if [ x$PRODUCT = x ]; then
        echo "ERROR: You need to set PRODUCT in Hudson"
        return 1
    fi
    if [ x$PRODUCT_VERSION = x ]; then
        echo "ERROR: You need to set PRODUCT_VERSION in Hudson"
        return 1
    fi
    if [ x$GFESB_KITROOT = x ]; then
        echo "ERROR: You need to set GFESB_KITROOT in Hudson"
        return 1
    fi

    echo "FINISHED: verifying variables"

    return 0
}


is_clean_time()
{
  hour=`date '+%H'`
  minute=`date '+%M'`

 # Clean out entire m2 directory if the build runs at 3am  
 if [ $hour -eq 3 ]; then
    if [ $minute -lt 16 ]; then
      # time to clean out the whole local m2 repo
      return 1
    fi
 fi

 return 0 
}


clean_whole_m2_repo()
{
    clean_errs=0
    cmd="rm -rf $SRCROOT/m2'"
    echo "in clean_whole_m2_repo  - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        clean_errs=1
        return $clean_errs
    fi

   return $clean_errs
}


clean_partial_m2_repo()
{
    clean_errs=0
    cmd="rm -rf $SRCROOT/m2/repository/com/stc/jmsjca'"
    echo "in clean_partial_m2_repo  - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        clean_errs=1
        return $clean_errs
    fi

   return $clean_errs
}

write_logs()
{
    write_logs_errs=0

    echo "STARTING: write bldvars.log"
#Write all variables to the BLDVARS_LOG
cat << EOF >> "$BLDVARS_LOG"
SRCROOT=${SRCROOT}
SVN_BRANCH=${SVN_BRANCH}
SVN_URL=${SVN_URL}
SVN_REVISON=${SVN_REVISION}
BUILD_TIMESTAMP=${BUILD_TIMESTAMP}
SMB_KIT_LOC=${SMB_KIT_LOC}
NFS_KIT_LOC=${NFS_KIT_LOC}
BUILD_NUMBER=${BLDNUM}
RELSTAGE=${RELSTAGE}
GFADDONS_SRCROOT=${GFADDONS_SRCROOT}
LOGROOT=${LOGROOT}
LOGDIR=${LOGDIR}
JAVA_HOME=${JAVA_HOME}
ANT_HOME=${ANT_HOME}
MAVEN_HOME=${MAVEN_HOME}
EOF
    echo "FINISHED: writing bldvars.log"

    echo "STARTING: write gfaddons.ver"

#Write all variables to the BLDVARS_LOG
cat << EOF >> "$GFADD_VER"
BUILD_NUMBER=${BLDNUM}
CODELINE=${SVN_BRANCH}
TIMESTAMP=${BUILD_TIMESTAMP}
EOF
    echo "FINISHED: writing gfaddons.ver"

    echo "STARTING: write gfesb_download"

cat << EOF >> "$GFESB_DOWNLOAD"
GF_Addons_BUILD_NUMBER="${BLDNUM}"
GF_Addons_CODELINE="${SVN_BRANCH}"
GF_Addons_TIMESTAMP="${BUILD_TIMESTAMP}"
GF_Addons_FULL_VERSION="${PRODUCT_VERSION}"
GF_Addons_FULL_PRODUCT_NAME="GLASSFISH ADDONS"
openesb-glassfish-addons-installer.zip=${GFESB_KITROOT}/${SVN_BRANCH}/nightly/Build${BLDNUM}/openesb-glassfish-addons-installer.zip 
openesb-glassfish-addons-installer.jar=${GFESB_KITROOT}/${SVN_BRANCH}/nightly/Build${BLDNUM}/openesb-glassfish-addons-installer.jar
EOF
    echo "FINISHED: writing gfesb_download"

    echo "STARTING: write srcupdate.log"
    cd $GFADDONS_SRCROOT
    svn info > "$SRCUP_LOG"
    echo "FINISHED: writing bldvars.log"

    return 0
}


java_clean()
{
    java_clean_errs=0

    ###########
    # run maven clean command
    ###########
    cd $SRCROOT/build/openesb-glassfish-addons
    cmd="mvn -Dmaven.test.skip=true -Dmaven.repo.local=${SRCROOT}/m2/repository -DPROJECTROOT=${SRCROOT}/build/openesb-glassfish-addons clean"
    echo "in java_clean - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        java_clean_errs=1
        return $java_clean_errs
    fi

    return $java_clean_errs
}


java_build()
{
    java_build_errs=0

    ###########
    # run maven build command
    ###########
    cd $SRCROOT/build/openesb-glassfish-addons
    cmd="mvn -Dmaven.test.skip=true -Dmaven.repo.local=${SRCROOT}/m2/repository -DPROJECTROOT=${SRCROOT}/build/openesb-glassfish-addons"
    echo "in java_build - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        java_build_errs=1
        return $java_clean_build
    fi

    return $java_build_errs
}


stage_locally()
{

    #Remove old staging dirs
    echo "Removing RELSTAGE : $RELSTAGE"
    rm -rf ${RELSTAGE}
    status=$?
    if [ $status -ne 0 ]; then
        echo Failed to remove $RELSTAGE
        return 1
    fi

    #Create new staging dirs
    echo mkdir -p ${RELSTAGE}/kits/gfaddons
    mkdir -p ${RELSTAGE}/kits/gfaddons
    status=$?
    if [ $status -ne 0 ]; then
        echo Failed to create $RELSTAGE/kits/gfaddons
        return 1
    fi

    ###
    #Stage GFADDONS  kit
    ###
    releaselist="${GFADDONS_SRCROOT}/packaging/build/openesb-glassfish-addons-installer.jar ${GFADDONS_SRCROOT}/packaging/build/openesb-glassfish-addons-installer.zip $GFADD_VER $GFESB_DOWNLOAD $SRCUP_LOG"
    echo "Releasing $releaselist to $RELSTAGE" 
    echo ${BLDDIR} > $RELSTAGE/kits/gfaddons/version.txt

    for releasefile in $releaselist
    do
       if [ ! -f $releasefile ]; then
           echo "ERROR - File: $releasefile does not exist.  Build has FAILED."
           return  1
       fi
       cp -pr $releasefile $RELSTAGE/kits/gfaddons/
   done

    return 0
}

make_mavenarchive()
{
    makemavenarchive_errs=0

    cmd="mkdir -p ${RELSTAGE}/maven"
    echo "in make_mavenarvhive - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        makemavenarchive_errs=1
        return $makemavenarchive_errs
    fi

    cmd="zip -r $RELSTAGE/maven/m2.zip $SRCROOT/m2"
    echo "in make_mavenarvhive - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        make_mavenarchive_errs=1
        return $makemavenarchive_errs
    fi
    echo "in make_mavenarvhive - finished running $cmd"

    return $makemavenarchive_errs
}

make_release()
{
    makerelease_errs=0

    cmd="mkdir -p ${NFS_KIT_LOC}/${PRODUCT}/${SVN_BRANCH}/${BLDDIR}/cmn"
    echo "in make_release - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        makerelease_errs=1
        return $makerelease_errs
    fi

    cmd="mkdir -p ${NFS_KIT_LOC}/${PRODUCT}/${SVN_BRANCH}/${BLDDIR}/maven"
    echo "in make_release - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        makerelease_errs=1
        return $makerelease_errs
    fi

    cmd="/opt/csw/bin/rsync -rtplv ${RELSTAGE}/kits/gfaddons/ ${NFS_KIT_LOC}/${PRODUCT}/${SVN_BRANCH}/${BLDDIR}/cmn"
    echo "in make_release - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        makerelease_errs=1
        return $makerelease_errs
    fi
    cmd="/opt/csw/bin/rsync -rtplv ${RELSTAGE}/maven/ ${NFS_KIT_LOC}/${PRODUCT}/${SVN_BRANCH}/${BLDDIR}/maven"
    echo "in make_release - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        makerelease_errs=1
        return $makerelease_errs
    fi

    # Create latest symlink in kits directory
    cmd="rm ${KITDIR}/.previouslatest"
    echo "removing .previouslatest symlink - `echo $cmd`"
    eval $cmd
    cmd="mv ${KITDIR}/latest ${KITDIR}/.previouslatest"
    echo "moving latest symlink to .previouslatest symlink - `echo $cmd`"
    eval $cmd
    cmd="ln -s ${KITDIR}/${BLDDIR} ${KITDIR}/latest"
    echo "adding new latest symlink - `echo $cmd`"
    eval $cmd

    return $makerelease_errs

}

push_kit()
{
    push_kit_errs=0

    pushkitlist="$RELSTAGE/kits/gfaddons/openesb-glassfish-addons-installer.jar $RELSTAGE/kits/gfaddons/openesb-glassfish-addons-installer.zip $RELSTAGE/kits/gfaddons/gfaddons.ver $RELSTAGE/kits/gfaddons/srcupdate.log $RELSTAGE/kits/gfaddons/gfesb_download"

    #####
    #test the ssh connection, by copying a file from local /tmp to remote /tmp:
    #####
    _pushkit_test=/tmp/pushkit_sshtest.$$
    touch -f $_pushkit_test
    scp -B $_pushkit_test ${PUSHKIT_IDENTITY}:$_pushkit_test
    if [ $? -ne 0 ]; then
        echo "in push_kit - Cannot copy test file '$_pushkit_test' using ssh identity '$PUSHKIT_IDENTITY' - ABORT"
        push_kit_errs=1
        rm -f $_pushkit_test
        return $push_kit_errs
    fi

    ###########
    #connection okay - remove local & remote test files, create destination directory:
    ###########
    rm -f $_pushkit_test
    cmd="ssh $PUSHKIT_IDENTITY 'rm -f $_pushkit_test && mkdir -p $PUSHKITLOC'"
    echo "in push_kit - running $cmd"
    eval "$cmd"
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        push_kit_errs=1
        return $push_kit_errs
    fi


    #copy kits.  note -B option is for "batch", and prevents looping for password entry:
    cmd="scp -B -pr $pushkitlist ${PUSHKIT_IDENTITY}:${PUSHKITLOC}"
    echo "in pushkit - running $cmd"
    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        echo "$cmd FAILED"
        push_kit_errs=1
        return $push_kit_errs
    else
        echo "copy to $PUSHKITLOC location succeeded"
    fi

    ####
    # Create latest symlink in PUSHKIT_DEST directory
    ####
    cmd="ssh $PUSHKIT_IDENTITY 'cd $PUSHKITLOC/.. && rm -f .previouslatest && ((test -d latest &&  mv latest .previouslatest) || rm -f latest) && ln -s ${BLDDIR} latest'"
    echo "in push_kit running $cmd"
    eval $cmd
    status=$?
    if [ $status -ne 0 ]; then
        echo  "$cmd FAILED"
        push_kit_errs=1
        return $push_kit_errs
    fi

    echo "finished push_kit"

    return $push_kit_errs
}

##############
#### MAIN ####
##############

###################
#Setting Variables#
###################

##############################################################################################################
#Set these statically in Hudson or the build will fail
#SRCROOT SVN_URL SVN_BRANCH SMB_KIT_LOC NFS_KIT_LOC PUSHKIT_DEST PUSHKIT_IDENTITY JAVA_HOME ANT_HOME MAVEN_HOME PATH PRODUCT PRODUCT_VERSION GFESB_KITROOT
##############################################################################################################

###########################################
#Set the rest of the variables dynamically# 
###########################################
# NOTE!! THESE ARE USED BY THE MAVEN COMMANDS : SVN_URL SVN_BRANCH BUILD_TIMESTAMP SVN_REVISION
###########################################
export BUILD_TIMESTAMP GFADDONS_SRCROOT LOGROOT LOGDIR BLDNUM BLDDIR RELSTAGE KITDIR PUSHKITLOC 
BUILD_TIMESTAMP=`date '+%Y.%m.%d.%H.%M.%S'`
yymmdd=`date  '+%y%m%d'`
datedir=${yymmdd}_${BUILD_NUMBER}
GFADDONS_SRCROOT=${SRCROOT}/build/openesb-glassfish-addons
LOGROOT=${SRCROOT}/stable/logs
LOGDIR=$LOGROOT/$datedir
BLDNUM=$datedir
BLDDIR=Build${BLDNUM}
RELSTAGE=$GFADDONS_SRCROOT/bld
KITDIR=${NFS_KIT_LOC}/${PRODUCT}/${SVN_BRANCH}
PUSHKITLOC=${PUSHKIT_DEST}/binaries/openesb-glassfish-addons/${SVN_BRANCH}/nightly/${BLDDIR}

########################
#Define and export logs#
########################
export WRITELOG_LOG VERIFYVARS_LOG CLEANMAVENREPO_LOG BLDVARS_LOG GFADD_VER GFESB_DOWNLOAD SRCUP_LOG LOCALSTAGE_LOG MAKEMAVENARCHIVE_LOG MAKERELEASE_LOG PUSHKIT_LOG JAVACLEANLOG JAVABUILD_LOG
WRITELOG_LOG=${LOGDIR}/writelog.log
VERIFYVARS_LOG=${LOGDIR}/verifyvars.log
CLEANMAVENREPO_LOG=${LOGDIR}/cleanmavenrepo.log
BLDVARS_LOG=${LOGDIR}/bldvars.log
GFADD_VER=${LOGDIR}/gfaddons.ver
GFESB_DOWNLOAD=${LOGDIR}/gfesb_download
SRCUP_LOG=${LOGDIR}/srcupdate.log
LOCALSTAGE_LOG=${LOGDIR}/localstage.log
MAKEMAVENARCHIVE_LOG=${LOGDIR}/makemavenarchive.log
MAKERELEASE_LOG=${LOGDIR}/makerelease.log
PUSHKIT_LOG=${LOGDIR}/pushkit.log
JAVACLEAN_LOG=${LOGDIR}/javaClean.log
JAVABUILD_LOG=${LOGDIR}/javaBuild.log

######################
#Create log directory#
######################
cmd="mkdir -p ${LOGDIR}"
eval "$cmd"
status=$?
if [ $status -ne 0 ]; then
    echo Build Error while creating $LOGDIR.  Build has FAILED.
    exit 1
fi

#Verify variables
verify_vars >> $VERIFYVARS_LOG 2>&1
status=$?
if [ $status -ne 0 ]; then
    echo verifyvars FAILED
    echo Build Error in verify_vars routine.  Build has FAILED.
    exit 1
fi

##Check Time to see what maven artifacts need to be cleaned
is_clean_time 
status=$?
if [ $status -ne 0 ]; then
    clean_whole_m2_repo >> $CLEANMAVENREPO_LOG 2>&1
    status=$?
    if [ $status -ne 0 ]; then
        echo "$clean_whole_m2_repo FAILED"
        exit 1
    fi
else
    clean_partial_m2_repo >> $CLEANMAVENREPO_LOG 2>&1
    status=$?
    if [ $status -ne 0 ]; then
        echo "clean_partial_m2_repo FAILED"
        exit 1
    fi
fi


#Write Additonal Log Files
write_logs >> $WRITELOG_LOG 2>&1
status=$?
if [ $status -ne 0 ]; then
    echo write_logs FAILED
    echo Build Error in write_logs routine.  Build has FAILED.
    exit 1
fi

#Run java clean
java_clean >> $JAVACLEAN_LOG 2>&1
status=$?
if [ $status -ne 0 ]; then
    echo Build Error in java_clean  routine.  Build has FAILED.
    exit 1
fi

#run java build
java_build >> $JAVABUILD_LOG 2>&1
status=$?
if [ $status -ne 0 ]; then
    echo Build Error in java_build routine.  Build has FAILED.
    exit 1
fi

#Stage files locally
stage_locally >> $LOCALSTAGE_LOG 2>&1
status=$?
if [ $status -ne 0 ]; then
    echo Build Error in stage_locally routine.  Build has FAILED.
    exit 1
fi

#run make_mavenarchive
make_mavenarchive >> $MAKEMAVENARCHIVE_LOG 2>&1
status=$?
if [ $status -ne 0 ]; then
    echo Build Error in make_mavenarchive routine.  Build has FAILED.
    exit 1
fi


#run make_release
make_release >> $MAKERELEASE_LOG 2>&1
status=$?
if [ $status -ne 0 ]; then
    echo Build Error in make_release routine.  Build has FAILED.
    exit 1
fi

##run push_kit
push_kit >> $PUSHKIT_LOG 2>&1
status=$?
if [ $status -ne 0 ]; then
    echo Build Error in push_kit routine.  Build has FAILED.
    exit 1
fi

