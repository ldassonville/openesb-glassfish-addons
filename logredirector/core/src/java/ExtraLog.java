/*
 * Created on Mar 26, 2009
 */

import org.glassfish.openesb.addons.logredirector.LogRedirectorLifeCycleListener;

/**
 * A copy of the lifecycle listener in the anonymous package that allows the user
 * to avoid a lot of typing of a long package and classname.
 * 
 * @author fkieviet
 */
public class ExtraLog extends LogRedirectorLifeCycleListener {
}
