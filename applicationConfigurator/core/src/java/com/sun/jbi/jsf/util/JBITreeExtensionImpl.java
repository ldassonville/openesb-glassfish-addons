/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.jbi.jsf.util;

import org.glassfish.openesb.addons.configurator.beans.DataBean;
import org.glassfish.openesb.addons.configurator.model.ConfigurableBean;
import org.glassfish.openesb.addons.configurator.util.BeansUtil;
import org.glassfish.openesb.addons.configurator.util.CapsConstants;
import com.sun.webui.jsf.component.ImageHyperlink;
import com.sun.webui.jsf.component.TreeNode;

import java.util.List;
import java.util.Map;


/**
 *  <p> The <code>JBITreeExtensionImpl</code> provides an
 *  extension implementation to populate the navigation tree in
 *  the GlassFish admin-gui web console.
 *  
 *  @author Sun Microsystems
 *  @version $Revision $
 *
 */
public class JBITreeExtensionImpl implements JBITreeExtension {
	private final static boolean IS_CLUSTER_PROFILE = ClusterUtilities
	.isClusterProfile();

	private final static String DATABEAN = "DataBean",
	CAPSCONFIGBEAN = "CAPSConfigurationBean",
	DATABEAN_TYPE = "org.glassfish.openesb.addons.configurator.beans.DataBean",
	CAPSCONFIGBEAN_TYPE = "org.glassfish.openesb.addons.configurator.beans.CAPSConfigurationBean",
	INSTANCEBEAN = "InstanceUIBean",
	CONNECTOR = "connectorConnectionPool",
	ENVIRONMENT = "environmentOverride";

	public void init(TreeNode anExtensionSubtreeRoot) {
		// NOT I18n
		String ROOT_TEXT = "ESB";
        String ROOT_TOOL_TIP = "ESB";
        String ROOT_URL = "caps/root.jsf";
        String contentPane = anExtensionSubtreeRoot.getTarget();
        anExtensionSubtreeRoot.setText("ESB");
        anExtensionSubtreeRoot.setToolTip("ESB");
        anExtensionSubtreeRoot.setUrl("caps/root.jsf");
        Map rootFacets = anExtensionSubtreeRoot.getFacets();
        ImageHyperlink imageHyperlink = new ImageHyperlink();
        imageHyperlink.setImageURL("/resource/images/folder.gif");
        imageHyperlink.setTarget(contentPane);
        imageHyperlink.setUrl("caps/root.jsf");
        rootFacets.put("image", imageHyperlink);
        createRootChildren(anExtensionSubtreeRoot);

	}
        
         public  void update(TreeNode treenode) {
             //doesn't support yet;
         }
               

	/**
	 * <p>Callback method that is called just before rendering takes place.
	 * This method will <strong>only</strong> be called for the page that
	 * will actually be rendered (and not, for example, on a page that
	 * handled a postback and then navigated to a different page).  Customize
	 * this method to allocate resources that will be required for rendering
	 * this page.</p>
	 */
	public void createRootChildren(TreeNode CAPSNode) {

		 String CONN_ROOT_TEXT = "Connector Connection Pools";
	        String CONN_ROOT_TOOL_TIP = "Connector Connection Pools";
	        String CONN_ROOT_URL = "/caps/connectionPools.jsf";
	        try
	        {
	            List outerChildren = CAPSNode.getChildren();
	            outerChildren.clear();
	            TreeNode connRootNode = new TreeNode();
	            connRootNode.setId("connRootNode");
	            connRootNode.setText("Connector Connection Pools");
	            connRootNode.setTarget(CAPSNode.getTarget());
	            connRootNode.setUrl("/caps/connectionPools.jsf");
	            connRootNode.setToolTip("Connector Connection Pools");
	            Map connRootFacets = connRootNode.getFacets();
	            ImageHyperlink connImageHyperlink = new ImageHyperlink();
	            connImageHyperlink.setImageURL("/resource/images/folder.gif");
	            connImageHyperlink.setTarget(CAPSNode.getTarget());
	            connImageHyperlink.setUrl("/caps/connectionPools.jsf");
	            connRootFacets.put("image", connImageHyperlink);
	            outerChildren.add(connRootNode);
	            DataBean dataBean = (DataBean)BeansUtil.getBean("DataBean", "org.glassfish.openesb.addons.configurator.beans.DataBean");
	            if(dataBean != null)
	            {
	                dataBean.setConnectorConnectionPoolTableData();
	                List connPools = dataBean.getCapsTreeData();
	                List connRootChildren = connRootNode.getChildren();
	                for(int i = 0; i < connPools.size(); i++)
	                {
	                    TreeNode poolNode = new TreeNode();
	                    poolNode.setId((new StringBuilder()).append("connTreeNode").append(i).toString());
	                    poolNode.setText(((ConfigurableBean)connPools.get(i)).getName());
	                    poolNode.setTarget(CAPSNode.getTarget());
	                    String url = (new StringBuilder()).append("/caps/configurationEditor.jsf?name=").append(((ConfigurableBean)connPools.get(i)).getName()).append("&type=").append("connectorConnectionPool").toString();
	                    poolNode.setUrl(url);
	                    Map poolFacets = poolNode.getFacets();
	                    ImageHyperlink imageHyperlink = new ImageHyperlink();
	                    imageHyperlink.setImageURL("/resource/images/caps/connectionPool.gif");
	                    imageHyperlink.setTarget(CAPSNode.getTarget());
	                    imageHyperlink.setUrl(url);
	                    poolFacets.put("image", imageHyperlink);
	                    connRootChildren.add(poolNode);
	                }

	            }
	        }
	        catch(Exception e) { }

	}
}
