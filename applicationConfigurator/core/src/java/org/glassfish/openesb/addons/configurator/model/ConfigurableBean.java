package org.glassfish.openesb.addons.configurator.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ConfigurableBean implements Comparator{
	private String name, encodedData;
	private boolean selected= false;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEncodedData() {
		return encodedData;
	}

	public void setEncodedData(String encodedData) {
		this.encodedData = encodedData;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
    public int compare(Object o1, Object o2)
    {
        return ((ConfigurableBean)o1).getName().compareTo(((ConfigurableBean)o2).getName());
    }


    public static void sort(List compInfoList)
    {
        try
        {
            Collections.sort(compInfoList, new Comparator() {

                public int compare(Object o1, Object o2)
                {
                    return ((ConfigurableBean)o1).getName().compareTo(((ConfigurableBean)o2).getName());
                }

            });
        }
        catch(ClassCastException ccEx) { }
        catch(UnsupportedOperationException unsupEx) { }
    }

}
