/*
  * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
  *
  * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
  *
  * The contents of this file are subject to the terms of either the GNU
  * General Public License Version 2 only ("GPL") or the Common Development
  * and Distribution License("CDDL") (collectively, the "License").  You
  * may not use this file except in compliance with the License. You can obtain
  * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
  * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
  * language governing permissions and limitations under the License.
  *
  * When distributing the software, include this License Header Notice in each
  * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
  * Sun designates this particular file as subject to the "Classpath" exception
  * as provided by Sun in the GPL Version 2 section of the License file that
  * accompanied this code.  If applicable, add the following below the License
  * Header, with the fields enclosed by brackets [] replaced by your own
  * identifying information: "Portions Copyrighted [year]
  * [name of copyright owner]"
  *
  * Contributor(s):
  *
  * If you wish your version of this file to be governed by only the CDDL or
  * only the GPL Version 2, indicate your decision by adding "[Contributor]
  * elects to include this software in this distribution under the [CDDL or GPL
  * Version 2] license."  If you don't indicate a single choice of license, a
  * recipient has the option to distribute your version of this file under
  * either the CDDL, the GPL Version 2 or to extend the choice of license to
  * its licensees as provided above.  However, if you add GPL Version 2 code
  * and therefore, elected the GPL Version 2 license, then the option applies
  * only if the new code is made subject to such option by the copyright
  * holder.
  */

package org.glassfish.openesb.addons.appverifier.cli.commands;

import com.sun.enterprise.cli.framework.CLILogger;
import com.sun.enterprise.cli.framework.CommandException;
import com.sun.enterprise.cli.framework.CommandValidationException;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;

import com.sun.enterprise.cli.commands.S1ASCommand;
import com.sun.enterprise.cli.framework.CommandException;
import com.sun.enterprise.cli.framework.CommandValidationException;

import java.util.Iterator;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * 
 * Commandline version of verifier. This class invokes the verifier MBean to 
 * verify the results. 
 * @author Sreeni Genipudi
 *
 */
public class CAPSVerifierCommand extends S1ASCommand {
    //Verifier MBean Object name
    protected static final String OBJ_NAME_CUSTOM_RESOURCES = 
        "com.sun.jbi:ServiceName=JavaEEVerifier,ComponentType=System";
    //Constants         
    private static final String REF_CLASS = "_referencerclass";
    private static final String REF_BY = "_referencer";
    private static final String JNDI_NAME = "_jndiname";


    private static final String EAR_FILE_NAME = "_filename";
    private static final String JNDI_CLASS_TYPE = "_jndinameClass";
    private static final String MESSAGE = "_message";
    private static final String STATUS = "_status";


    private static final String SU_FILE = "Service Unit file";


    private static final String ERRORMSG = "ERROR_MSG";
    private static final String WARNINGMSG = "WARNING_MSG";
    private static final String OKMSG = "OK_MSG";
    private static final String DELIM = ":";
    private static final String DISP_REF_CLASS = "Disp_Referrence_Class";
    private static final String DISP_REF_BY = "Disp_Referrence_By";
    private static final String DISP_JNDI_NAME = "Disp_JNDI_Name";


    private static final String DISP_EAR_FILE_NAME = "Disp_Ear_Filename";
    private static final String DISP_JNDI_CLASS_TYPE = "Disp_JNDI_Class_Type";
    private static final String DISP_MESSAGE = "Disp_Message";
    private static final String DISP_STATUS = "Disp_Status";

    /* (non-Javadoc)
     * @see com.sun.enterprise.cli.framework.Command#runCommand()
     */

    @Override
    public void runCommand() throws CommandException, 
                                    CommandValidationException {
        validateOptions();

        String host = getHost();
        int port = getPort();
        String user = getUser();
        String passwd = getPassword();

        MBeanServerConnection mbsc = 
            getMBeanServerConnection(host, port, user, passwd);
        try {

            String earFile = getEARFile();
            Map clOptionsMap = this.getCLOptions();
            String instance = null;
            if (clOptionsMap != null) {
                instance = (String)this.getCLOptions().get("target");
            }
            if (instance == null) {
                instance = "server";
            }
            //If the third parameter is null this is 
            // just verification of EAR file without SA.


            //test that the server is running
            // TODO: is there a better way?
            TabularData tb = 
                (TabularData)mbsc.invoke(new ObjectName(OBJ_NAME_CUSTOM_RESOURCES), 
                                         "verifyServiceUnit", 
                                         new Object[] { "", earFile, 
                                                        instance }, 
                                         new String[] { "java.lang.String", 
                                                        "java.lang.String", 
                                                        "java.lang.String" });
            Collection col = tb.values();
            Iterator itr = col.iterator();
            StringBuffer strBuff = null;
            String earName = null;
            String jndiName = null;
            String referredBy = null;
            String referredClass = null;
            String message = null;
            String status = null;
            ResourceBundle rb = 
                ResourceBundle.getBundle("org.glassfish.openesb.addons.appverifier.Bundle", 
                                         Locale.getDefault());
            String earFileNameKey = rb.getString(this.DISP_EAR_FILE_NAME);
            String jndiNameKey = rb.getString(this.DISP_JNDI_NAME);
            String refByKey = rb.getString(this.DISP_REF_BY);
            String refClassKey = rb.getString(this.DISP_REF_CLASS);
            String messageKey = rb.getString(this.DISP_MESSAGE);
            String statusKey = rb.getString(this.DISP_STATUS);

            while (itr.hasNext()) {

                strBuff = new StringBuffer();
                CompositeData cd = (CompositeData)itr.next();
                earName = (String)cd.get(earFileNameKey);
                jndiName = (String)cd.get(jndiNameKey);
                referredBy = (String)cd.get(refByKey);
                referredClass = (String)cd.get(refClassKey);
                message = (String)cd.get(messageKey);
                status = (String)cd.get(statusKey);

                strBuff.append(earFileNameKey);
                strBuff.append(DELIM);
                strBuff.append(earName);
                strBuff.append("\n");
                if (jndiName != null && !jndiName.equals("")) {
                    strBuff.append(jndiNameKey);
                    strBuff.append(DELIM);
                    strBuff.append(jndiName);
                    strBuff.append("\n");

                }
                if (referredBy != null && !referredBy.equals("")) {
                    strBuff.append(refByKey);
                    strBuff.append(DELIM);
                    strBuff.append(referredBy);
                    strBuff.append("\n");
                }
                if (referredClass != null && !referredClass.equals("")) {
                    strBuff.append(refClassKey);
                    strBuff.append(DELIM);
                    strBuff.append(referredClass);
                    strBuff.append("\n");
                }

                if (message != null && !message.equals("")) {
                    strBuff.append(messageKey);
                    strBuff.append(DELIM);
                    strBuff.append(message);
                    strBuff.append("\n");
                }

                strBuff.append(statusKey);
                strBuff.append(DELIM);
                strBuff.append(status);
                strBuff.append("\n");
                CLILogger.getInstance().printMessage(strBuff.toString());

            }

            CLILogger.getInstance().printDetailMessage(getLocalizedString("CommandSuccessful", 
                                                                          new Object[] { name }));
        } catch (Exception e) {
            displayExceptionMessage(e);
            //throw new CommandException(e);
        }
    }


    private String getEARFile() throws CommandValidationException {
        String ef = (String)getOperands().get(0);
        return ef;
    }
}
