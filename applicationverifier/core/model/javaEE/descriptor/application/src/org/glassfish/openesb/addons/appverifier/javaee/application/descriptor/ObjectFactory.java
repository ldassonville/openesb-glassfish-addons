/* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-382 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2007.10.16 at 11:22:38 PM PDT 
//


package org.glassfish.openesb.addons.appverifier.javaee.application.descriptor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.stc.applicationVerifier.javaee.application.descriptor package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _Application_QNAME = 
        new QName("http://java.sun.com/xml/ns/javaee", "application");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.stc.applicationVerifier.javaee.application.descriptor
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.XsdPositiveIntegerType}
     * 
     */
    public XsdPositiveIntegerType createXsdPositiveIntegerType() {
        return new XsdPositiveIntegerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.LocalType}
     * 
     */
    public LocalType createLocalType() {
        return new LocalType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.MessageDestinationLinkType}
     * 
     */
    public MessageDestinationLinkType createMessageDestinationLinkType() {
        return new MessageDestinationLinkType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.EjbLocalRefType}
     * 
     */
    public EjbLocalRefType createEjbLocalRefType() {
        return new EjbLocalRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.XsdStringType}
     * 
     */
    public XsdStringType createXsdStringType() {
        return new XsdStringType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.TrueFalseType}
     * 
     */
    public TrueFalseType createTrueFalseType() {
        return new TrueFalseType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ServiceRefType}
     * 
     */
    public ServiceRefType createServiceRefType() {
        return new ServiceRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.RunAsType}
     * 
     */
    public RunAsType createRunAsType() {
        return new RunAsType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.LifecycleCallbackType}
     * 
     */
    public LifecycleCallbackType createLifecycleCallbackType() {
        return new LifecycleCallbackType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ResAuthType}
     * 
     */
    public ResAuthType createResAuthType() {
        return new ResAuthType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.EnvEntryType}
     * 
     */
    public EnvEntryType createEnvEntryType() {
        return new EnvEntryType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.XsdAnyURIType}
     * 
     */
    public XsdAnyURIType createXsdAnyURIType() {
        return new XsdAnyURIType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.HomeType}
     * 
     */
    public HomeType createHomeType() {
        return new HomeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.XsdIntegerType}
     * 
     */
    public XsdIntegerType createXsdIntegerType() {
        return new XsdIntegerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ServiceRefHandlerChainsType}
     * 
     */
    public ServiceRefHandlerChainsType createServiceRefHandlerChainsType() {
        return new ServiceRefHandlerChainsType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.JavaIdentifierType}
     * 
     */
    public JavaIdentifierType createJavaIdentifierType() {
        return new JavaIdentifierType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.XsdNonNegativeIntegerType}
     * 
     */
    public XsdNonNegativeIntegerType createXsdNonNegativeIntegerType() {
        return new XsdNonNegativeIntegerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ModuleType}
     * 
     */
    public ModuleType createModuleType() {
        return new ModuleType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.MessageDestinationUsageType}
     * 
     */
    public MessageDestinationUsageType createMessageDestinationUsageType() {
        return new MessageDestinationUsageType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ParamValueType}
     * 
     */
    public ParamValueType createParamValueType() {
        return new ParamValueType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ResSharingScopeType}
     * 
     */
    public ResSharingScopeType createResSharingScopeType() {
        return new ResSharingScopeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.MessageDestinationRefType}
     * 
     */
    public MessageDestinationRefType createMessageDestinationRefType() {
        return new MessageDestinationRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.RemoteType}
     * 
     */
    public RemoteType createRemoteType() {
        return new RemoteType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.MessageDestinationType}
     * 
     */
    public MessageDestinationType createMessageDestinationType() {
        return new MessageDestinationType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.DescriptionType}
     * 
     */
    public DescriptionType createDescriptionType() {
        return new DescriptionType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.PersistenceUnitRefType}
     * 
     */
    public PersistenceUnitRefType createPersistenceUnitRefType() {
        return new PersistenceUnitRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.FullyQualifiedClassType}
     * 
     */
    public FullyQualifiedClassType createFullyQualifiedClassType() {
        return new FullyQualifiedClassType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ApplicationType}
     * 
     */
    public ApplicationType createApplicationType() {
        return new ApplicationType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.JndiNameType}
     * 
     */
    public JndiNameType createJndiNameType() {
        return new JndiNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ServiceRefHandlerChainType}
     * 
     */
    public ServiceRefHandlerChainType createServiceRefHandlerChainType() {
        return new ServiceRefHandlerChainType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ListenerType}
     * 
     */
    public ListenerType createListenerType() {
        return new ListenerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.DisplayNameType}
     * 
     */
    public DisplayNameType createDisplayNameType() {
        return new DisplayNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.EjbRefNameType}
     * 
     */
    public EjbRefNameType createEjbRefNameType() {
        return new EjbRefNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.SecurityRoleType}
     * 
     */
    public SecurityRoleType createSecurityRoleType() {
        return new SecurityRoleType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ResourceEnvRefType}
     * 
     */
    public ResourceEnvRefType createResourceEnvRefType() {
        return new ResourceEnvRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.GenericBooleanType}
     * 
     */
    public GenericBooleanType createGenericBooleanType() {
        return new GenericBooleanType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ServiceRefHandlerType}
     * 
     */
    public ServiceRefHandlerType createServiceRefHandlerType() {
        return new ServiceRefHandlerType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.XsdBooleanType}
     * 
     */
    public XsdBooleanType createXsdBooleanType() {
        return new XsdBooleanType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.JavaTypeType}
     * 
     */
    public JavaTypeType createJavaTypeType() {
        return new JavaTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.PersistenceContextTypeType}
     * 
     */
    public PersistenceContextTypeType createPersistenceContextTypeType() {
        return new PersistenceContextTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.PersistenceContextRefType}
     * 
     */
    public PersistenceContextRefType createPersistenceContextRefType() {
        return new PersistenceContextRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.XsdNMTOKENType}
     * 
     */
    public XsdNMTOKENType createXsdNMTOKENType() {
        return new XsdNMTOKENType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.EmptyType}
     * 
     */
    public EmptyType createEmptyType() {
        return new EmptyType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.WebType}
     * 
     */
    public WebType createWebType() {
        return new WebType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.InjectionTargetType}
     * 
     */
    public InjectionTargetType createInjectionTargetType() {
        return new InjectionTargetType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.String}
     * 
     */
    public String createString() {
        return new String();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.MessageDestinationTypeType}
     * 
     */
    public MessageDestinationTypeType createMessageDestinationTypeType() {
        return new MessageDestinationTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.EjbRefTypeType}
     * 
     */
    public EjbRefTypeType createEjbRefTypeType() {
        return new EjbRefTypeType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.IconType}
     * 
     */
    public IconType createIconType() {
        return new IconType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.SecurityRoleRefType}
     * 
     */
    public SecurityRoleRefType createSecurityRoleRefType() {
        return new SecurityRoleRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.RoleNameType}
     * 
     */
    public RoleNameType createRoleNameType() {
        return new RoleNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.EnvEntryTypeValuesType}
     * 
     */
    public EnvEntryTypeValuesType createEnvEntryTypeValuesType() {
        return new EnvEntryTypeValuesType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.PathType}
     * 
     */
    public PathType createPathType() {
        return new PathType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.UrlPatternType}
     * 
     */
    public UrlPatternType createUrlPatternType() {
        return new UrlPatternType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.EjbLinkType}
     * 
     */
    public EjbLinkType createEjbLinkType() {
        return new EjbLinkType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.EjbRefType}
     * 
     */
    public EjbRefType createEjbRefType() {
        return new EjbRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.XsdQNameType}
     * 
     */
    public XsdQNameType createXsdQNameType() {
        return new XsdQNameType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ResourceRefType}
     * 
     */
    public ResourceRefType createResourceRefType() {
        return new ResourceRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.PortComponentRefType}
     * 
     */
    public PortComponentRefType createPortComponentRefType() {
        return new PortComponentRefType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.PropertyType}
     * 
     */
    public PropertyType createPropertyType() {
        return new PropertyType();
    }

    /**
     * Create an instance of {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.LocalHomeType}
     * 
     */
    public LocalHomeType createLocalHomeType() {
        return new LocalHomeType();
    }

    /**
     * Create an instance of {@link JAXBElement} {@code <} {@link org.glassfish.openesb.addons.appverifier.javaee.application.descriptor.ApplicationType} {@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://java.sun.com/xml/ns/javaee", 
                    name = "application")
    public JAXBElement<ApplicationType> createApplication(ApplicationType value) {
        return new JAXBElement<ApplicationType>(_Application_QNAME, 
                                                ApplicationType.class, null, 
                                                value);
    }

}
