package org.glassfish.openesb.addons.appverifier;
/* *************************************************************************
  *
  *          Copyright (c) 2007, Sun Microsystems,
  *          All Rights Reserved
  *
  *          This program, and all the routines referenced herein,
  *          are the proprietary properties and trade secrets of
  *          Sun Microsystems.
  *
  *          Except as provided for by license agreement, this
  *          program shall not be duplicated, used, or disclosed
  *          without  written consent signed by an officer of
  *          Sun Microsystems.
  *
  ***************************************************************************/
/**
 * Enumeration of Verification Status
 * @author Sreeni Genipudi
 */
public enum VerifyStatus {
    OK,
    WARNING,
    ERROR,
    ;
}
