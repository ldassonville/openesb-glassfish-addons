/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;

import org.glassfish.openesb.addons.appverifier.ra.InboundResourceAdapter;

import org.glassfish.openesb.addons.appverifier.ra.OutboundResourceAdapter;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Outbound RA validator
 * @author Sreeni Genipudi
 */
public class OutboundRARValidator {
    //class instance
    private static OutboundRARValidator mInstance = null;
    //constant
    private static final String LOCALRARMARKER = "#";
    //logger
    private static Logger logger = 
        Logger.getLogger("org.glassfish.openesb.addons.appverifier.OutboundRARValidator");
    /**
     * Constructor
     */
    private OutboundRARValidator() {
    }

    /**
     * Get instance
     * @return instance
     */
    public static OutboundRARValidator getInstance() {
        if (mInstance == null) {
            mInstance = new OutboundRARValidator();
        }
        return mInstance;
    }

    /**
     * Is resource local
     * @param jndiName
     * @return boolean
     */
    public boolean isResourceLocal(String jndiName) {
        if (jndiName.indexOf(LOCALRARMARKER) != -1) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Is valid local resource 
     * @param jndiName
     * @param resourceType
     * @param outboundRARlist
     * @param err
     * @return boolean - is valid local resource
     */
    public boolean isValidLocalResource(String jndiName, String resourceType, 
                                        List<OutboundResourceAdapter> outboundRARlist, 
                                        ErrorBundle err) {
        String[] connectionIntfArry = null;
        boolean bJNDINameFound = false;
        for (OutboundResourceAdapter outRAR: outboundRARlist) {
            connectionIntfArry = outRAR.getConnectionInterface();
            String[] jndiNm = outRAR.getJNDIName();

            if (matchJNDIName(jndiName, jndiNm)) {
                bJNDINameFound = true;
                for (String connectionIntf: connectionIntfArry) {
                    if (resourceType.equals(connectionIntf)) {
                        return true;
                    } else {
                        logger.fine("DIDNT MATCH - LOOKING FOR MATCH RESOURCE TYPE = " + 
                                    resourceType + " Connection Interface " + 
                                    connectionIntf);
                    }
                }

                return false;
            }
        }
        if (bJNDINameFound) {
            err.addError("JNDI name found " + jndiName + 
                         "but the associated connection interface is wrong " + 
                         resourceType);
            err.addError(" Valid connection interface values are ");
            for (String connectionIntf: connectionIntfArry) {
                if (resourceType.equals(connectionIntf)) {
                    return true;
                } else {
                    err.addError("/t" + connectionIntf);
                }
            }
        } else {
            err.addError(" Invalid JNDI name " + jndiName);

        }
        return false;
    }


    private boolean matchJNDIName(String jndiName, String[] jndiNames) {
        for (String jndi: jndiNames) {
            if (jndi.equals(jndiName)) {
                return true;
            }
        }
        return false;
    }
}

