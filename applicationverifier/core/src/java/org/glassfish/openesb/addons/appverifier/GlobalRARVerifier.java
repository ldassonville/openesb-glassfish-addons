/* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */


package org.glassfish.openesb.addons.appverifier;

import java.io.IOException;

import java.util.*;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanParameterInfo;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.Reference;

/**
 *
 * @author Sreeni Genipudi
 */
public class GlobalRARVerifier {

    //    private MBeanServerConnection mbsc;
    private MBeanServerConnection mbsc;
    private ObjectName mObjectName;
    private ObjectName mJndiObjectName = null;
    private ObjectName mInstanceObjectName = null;
    private static Logger logger = 
        Logger.getLogger("org.glassfish.openesb.addons.appverifier.GlobalRARVerifier");
    private static final String JNDI_MONITOR_SERVER = 
        "com.sun.appserv:type=jndi,category=monitor,server=server";
    private static final String RESOURCE_OBJ_NAME = 
        "com.sun.appserv:type=resources,category=config";
    private static final String INSTANCE_SERVERS_CONFIG = 
        "com.sun.appserv:type=servers,category=config";


    private InitialContext mIx = null;
    private String mInstance = "server";
    /*    public static void main(String[] args) {

        String param = "eventMGMTXX";//args[0];

        try {
            String host = "localhost";
            String port = "8686";
            String username = "admin";
            String password = "adminadmin";

            GlobalRARVerifier invoker =
                new GlobalRARVerifier(host, port, username, password);

            //ObjectName listResults = invoker.listMonitor(new String[]{ param });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/

    public GlobalRARVerifier(MBeanServer mbeanServer, 
                             String target) throws Exception {
        mbsc = mbeanServer;
        mObjectName = new ObjectName(RESOURCE_OBJ_NAME);
        mJndiObjectName = new ObjectName(this.JNDI_MONITOR_SERVER);
        if (target != null && !target.equals(this.mInstance)) {
            mInstance = target;
            ObjectName obName = new ObjectName(this.INSTANCE_SERVERS_CONFIG);
            try {
                this.mInstanceObjectName = 
                        (ObjectName)mbsc.invoke(obName, "getServerByName", 
                                                new Object[] { mInstance }, 
                                                new String[] { "java.lang.String" });
            } catch (Throwable ee) {
                this.logger.log(Level.WARNING, "invalid target using default ", 
                                ee);
                mInstanceObjectName = null;
            }
        }

        try {
            mIx = new InitialContext();
        } catch (Exception ex) {

        }
    }


    public GlobalRARVerifier(MBeanServer mbeanServer) throws Exception {
        mbsc = mbeanServer;
        mObjectName = new ObjectName(RESOURCE_OBJ_NAME);
        mJndiObjectName = new ObjectName(this.JNDI_MONITOR_SERVER);
        try {
            mIx = new InitialContext();
        } catch (Exception ex) {

        }
    }

    public GlobalRARVerifier(String host, String port, String username, 
                             String password, String target) throws Exception {

        mbsc = getMBeanServerConnection(host, port, username, password);
        mObjectName = new ObjectName(RESOURCE_OBJ_NAME);
        mJndiObjectName = new ObjectName(this.JNDI_MONITOR_SERVER);
        if (target != null && !target.equals(this.mInstance)) {
            mInstance = target;
            ObjectName obName = new ObjectName(this.INSTANCE_SERVERS_CONFIG);
            try {
                this.mInstanceObjectName = 
                        (ObjectName)mbsc.invoke(obName, "getServerByName", 
                                                new Object[] { mInstance }, 
                                                new String[] { "java.lang.String" });
            } catch (Throwable ee) {
                this.logger.log(Level.WARNING, "invalid target using default ", 
                                ee);
                mInstanceObjectName = null;
            }
        }


        try {
            mIx = new InitialContext();
        } catch (Exception ex) {

        }
    }

    public String[] getAllGlobalRARJNDINames() throws Exception {
        ArrayList<String> listOfResources = null;
        String[] globalRARs = 
            (String[])mbsc.invoke(mObjectName, "getConnectorResourceNamesList", 
                                  null, null);
        if (this.mInstanceObjectName != null) {
            try {
                String[] resArray = 
                    (String[])mbsc.invoke(this.mInstanceObjectName, 
                                          "getResourceRefNamesList", null, 
                                          null);
                listOfResources = new ArrayList<String>();
                for (String jndi: globalRARs) {
                    listOfResources.add(jndi);
                }
                for (String jndi: resArray) {
                    listOfResources.add(jndi);
                }
                return listOfResources.toArray(new String[0]);
            } catch (Exception ee) {

            }

        }
        return globalRARs;
        //return globalRARs;
    }


    public List<String> getAllGlobalRARMID(String[] globalRARs) throws Exception {
        ArrayList<String> listOfGlobalRARMids = new ArrayList<String>();
        for (String jndiName: globalRARs) {
            ObjectName objname = 
                (ObjectName)mbsc.invoke(mObjectName, "getConnectorResourceByJndiName", 
                                        new Object[] { jndiName }, 
                                        new String[] { "java.lang.String" });
            logger.fine(" OBject name - " + objname);
            Object attrObj;
            try {
                attrObj = mbsc.getAttribute(objname, "pool-name");
            } catch (Exception ex1) {
                // May be this is not inbound RA
                continue;
            }
            logger.fine("Pool name =" + attrObj);
            objname = 
                    (ObjectName)mbsc.invoke(mObjectName, "getConnectorConnectionPoolByName", 
                                            new Object[] { (String)attrObj }, 
                                            new String[] { "java.lang.String" });
            attrObj = mbsc.getAttribute(objname, "resource-adapter-name");
            listOfGlobalRARMids.add((String)attrObj);
        }
        return listOfGlobalRARMids;

    }


    public Object[] getAllJNDINamesWithConnectionTypeAndMIDs(String[] globalRARs) throws Exception {
        HashMap<String, String> mapJndiNamesWithConnectionType = 
            new HashMap<String, String>();
        ArrayList<String> listOfGlobalRARMids = new ArrayList<String>();
        for (String jndiName: globalRARs) {
            ObjectName objname = 
                (ObjectName)mbsc.invoke(mObjectName, "getConnectorResourceByJndiName", 
                                        new Object[] { jndiName }, 
                                        new String[] { "java.lang.String" });
            logger.fine(" OBject name - " + objname);
            Object attrObj;
            try {
                attrObj = mbsc.getAttribute(objname, "pool-name");
            } catch (Exception ex1) {
                // May be this is not inbound RA
                continue;
            }
            logger.fine("Pool name =" + attrObj);
            objname = 
                    (ObjectName)mbsc.invoke(mObjectName, "getConnectorConnectionPoolByName", 
                                            new Object[] { (String)attrObj }, 
                                            new String[] { "java.lang.String" });
            attrObj = mbsc.getAttribute(objname, "connection-definition-name");
            mapJndiNamesWithConnectionType.put(jndiName, (String)attrObj);

            attrObj = mbsc.getAttribute(objname, "resource-adapter-name");
            listOfGlobalRARMids.add((String)attrObj);
        }
        return new Object[] { mapJndiNamesWithConnectionType, 
                              listOfGlobalRARMids };

    }

    private Object getMonitor(String param) throws Exception {
        Object[] objectList = new Object[] { param };
        String[] signatureList = new String[] { "java.lang.String" };

        Object result = 
            mbsc.invoke(mObjectName, "dottedNameMonitoringGet", objectList, 
                        signatureList);

        return result;
    }

    public boolean isValidJNIName(String jndiName) {
        //InitialContext ix;
        try {
            //ix = new InitialContext();
            mIx.lookup(jndiName);
            return true;
        } catch (NamingException e) {
            // TODO
            logger.log(Level.FINE, e.toString(), e);
        }
        return false;
        /*
        boolean bReturn = false;
        ArrayList<javax.naming.NameClassPair> listOfRefs = null;
        if ( jndiName == null ) {
            return false;
        }
        try {
            String delim = "/";
            String lastToken = null;
            int ind1 =jndiName.lastIndexOf(delim);


            String key = null;
            if  ( ind1 != -1) {
                key = jndiName.substring(0, ind1);
                lastToken = jndiName.substring(ind1+1);
            } else {
                key = jndiName;
            }
            listOfRefs =
                    (ArrayList<javax.naming.NameClassPair>) mbsc.invoke(this.mJndiObjectName,
            "getNames", new Object[]{key}, new String[] {"java.lang.String"});

            if ( lastToken != null ) {
                for ( javax.naming.NameClassPair reference: listOfRefs) {
                    if ( reference.getName().equals(lastToken)) {
                        bReturn = true;
                        break;
                    }
                }
            } else {
                bReturn = true;
            }
            return bReturn;
        } catch (MBeanException e) {
            this.logger.log(Level.WARNING,e.toString(),e);
        } catch (IOException e) {
             this.logger.log(Level.WARNING,e.toString(),e);
        } catch (InstanceNotFoundException e) {
             this.logger.log(Level.WARNING,e.toString(),e);
        } catch (ReflectionException e) {
             this.logger.log(Level.WARNING,e.toString(),e);
        }
        return bReturn;*/
    }


    private MBeanServerConnection getMBeanServerConnection(String host, 
                                                           String port, 
                                                           String user, 
                                                           String password) throws Exception {

        final JMXServiceURL url = 
            new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + ":" + 
                              port + "/jmxrmi");
        final Map env = new HashMap();
        final String PKGS = "com.sun.enterprise.admin.jmx.remote.protocol";

        env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, PKGS);
        //JMXConnectorFactory.
        env.put(JMXConnector.CREDENTIALS, new String[] { user, password });
        //"username", user);
        //env.put("password", password);
        env.put("com.sun.enterprise.as.http.auth", "BASIC");
        logger.fine(" URL = " + url);
        final JMXConnector conn = JMXConnectorFactory.connect(url, env);

        return conn.getMBeanServerConnection();
    }

}
