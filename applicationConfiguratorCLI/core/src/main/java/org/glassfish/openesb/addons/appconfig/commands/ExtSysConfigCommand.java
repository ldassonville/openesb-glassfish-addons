/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtSysConfigCommand.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig.commands;

import com.sun.enterprise.cli.commands.S1ASCommand;
import com.sun.enterprise.cli.framework.CLILogger;
import com.sun.enterprise.cli.framework.CommandException;
import com.sun.enterprise.cli.framework.CommandValidationException;
import java.io.File;
import java.util.logging.Level;

/**
 * earConfiguratorCommand base command class 
 */
public abstract class ExtSysConfigCommand extends S1ASCommand {

    protected final static String CONFIG_DIR = "cfgdir";
    protected final static String FORCE = "force";
    protected final static String DEFAULT_CONFIG_DIR = "config";
    protected static final String OBJ_NAME_CUSTOM_RESOURCES = "com.sun.appserv:type=resources,category=config";

    /**
     * Creates a new instance of EarConfiguratorCommand
     */
    public ExtSysConfigCommand() {
    }

    protected String getConfigDirOption()
            throws CommandValidationException {
        String configDir = getOption(CONFIG_DIR);
        if (configDir == null || configDir.equals("")) {
            configDir = DEFAULT_CONFIG_DIR;
        }
        return configDir;
    }

    protected boolean getOverwriteOption()
            throws CommandValidationException {

        return getBooleanOption(FORCE);
    }

    protected void validateConfigDir(String configDir) throws CommandException {
        File f = new File(configDir);
        if (f.exists() && (!f.isDirectory())) {
            throw new CommandException(getLocalizedString("ConfigDirIsAFile",
                    new Object[]{configDir}));
        }
    }

    protected void validateEarPath(String earPath) throws CommandException {
        File f = new File(earPath);
        if( (!f.getName().endsWith(".ear")) 
                || (!f.exists()) ){
            throw new CommandException(getLocalizedString("InvalidEar",
                    new Object[]{earPath}));
        }
    }

    protected void handleException(String name, Exception e) throws CommandException {

        Level outputLevel = CLILogger.getInstance().getOutputLevel();

        if (outputLevel != Level.FINEST) {
            CLILogger.getInstance().printMessage(
                    getLocalizedString("CommandUnSuccessfulWithErrorMsg",
                    new Object[]{name, e}));
        } else {
            displayExceptionMessage(e);
        }

    }
}
