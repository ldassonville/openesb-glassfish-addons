/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigImporter.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import javax.management.*;
import java.util.*;
import java.util.ArrayList;
import javax.xml.xpath.*;
import org.w3c.dom.*;

/**
 *
 * @author Sun Microsystems
 */
public class ConfigImporter {

    private JNDIHandler jh;
    private ConfigDirHandler cdh;

    private static final String OPTIONS = Constants.JMS_OPTIONS;
    
    // Some property files may have un-encrypted password values. These
    // clear-text values will be encrypted during import. 
    // Corresponding files need to be updated with the encrypted values.
    // Map<extsysid, list-of-updated-CAPSProperty-objects>
    Map<String, List<CAPSProperty>> mapFilesToUpdate;

    private XPathFactory xpf = XPathFactory.newInstance();
    private XPath xp = xpf.newXPath();

    public ConfigImporter(String configDir, MBeanServerConnection mbsc) {
        jh = new JNDIHandler(mbsc);
        cdh = new ConfigDirHandler(configDir);
    }

    public void doImport() throws Exception {

        // get all Configuration values from property files.
        // Map<extSysId, Configuration>
        Map<String, String> mapconfigs = cdh.getAllConfigurations();

        // Allocate new map to keep track of files that would need to
        // to updated.
        mapFilesToUpdate = new HashMap<String, List<CAPSProperty>>();

        Set<String> extSysIds = mapconfigs.keySet();
        for (Iterator<String> it = extSysIds.iterator(); it.hasNext();) {

            String extSysId = it.next();

            String configuration = mapconfigs.get(extSysId);

            Map<String, CAPSProperty> mapsFileProps = cdh.getCAPSProperties(extSysId);
            
            // ESConfig: representation of external system configuration template.
            // JMS template does not have <instance> section.
            // All others have both <template> as well as <instance>.
            ESConfig esc = new ESConfig(configuration);

            // import new configuration into JNDI
            if ( esc.hasInstance() ) {
                
                // Its not a JMS import.
                
                // Get updated configuration string. Call to update will also
                // encrypt any clear-text file values which need to be encrypted.
                configuration = update(esc, extSysId, mapsFileProps, Constants.ENC07);
                
                if (extSysId.startsWith(Constants.JCA_PREFIX)) {
                    extSysId = extSysId.substring(Constants.JCA_PREFIX.length());
                    jh.updateJCAConfig(extSysId, configuration);
                } else {
                    jh.importExternalSystemConfig(extSysId, configuration);
                }
                
            } else { 
                
                // Its a JMS import. Import indiviual properties.
                
                // Call update so that any clear-text file values which need to be
                // encrypted are encrypted and updated in mapFileProps.
                update(esc, extSysId, mapsFileProps, Constants.ENC08);
                
                // now create a java.util.Properties Object which will be 
                // imported.
                Properties propsObj = getJMSPropertiesObject(extSysId, mapsFileProps);
                
                if (extSysId.startsWith(Constants.JCA_PREFIX)) {
                    extSysId = extSysId.substring(Constants.JCA_PREFIX.length());
                    jh.updateJMSJCAConfig(extSysId, propsObj);
                } else {
                    jh.importJMSExternalSystemConfig(extSysId, propsObj);
                }
            }
        }

        // update property files that need update if any
        cdh.write(mapFilesToUpdate, true);

    }

    private Properties getJMSPropertiesObject(String extSysId, Map<String, CAPSProperty> cprops) throws Exception {
        Properties propsObj = new Properties();
        Collection<CAPSProperty> c = cprops.values();
        for (Iterator<CAPSProperty> it = c.iterator(); it.hasNext();) {
            CAPSProperty cprop = it.next();
            String key = cprop.getKey();
            
            if ( !Constants.ExtSysId_KEY.equals(key) ) {
                
                String value = cprop.getValue();
                
                if (key.equals(OPTIONS)) {
//                    print("import: Options file value: " + value);
                    if (extSysId.startsWith(Constants.JCA_PREFIX)) {
                        // pack Options in JMSJCA.sep=, format for JMS JCA
                        value = ConfigUtil.packOptions(null, value);
                    } else {
                        // encode multi-line vlaue into B64-GZ if its not the JCA
                        value = ConfigUtil.encodeB64GZ(value);
                    }
                    //print("import: Options import value: " + value);
                }
                
                propsObj.put(key, value);
            }
        }

        return propsObj;
    }

    /**
     * Get Configuration String with param values updated from property file 
     * values. This method has a side-effect of updaing file-values (in the
     * Map 'mapFileProps') if those values are encrypted during this update (see
     * descrpition of 'encryption' parameter below). 
     * 
     * @param esc ESConfig object. Representation of Configuration 
     * <template> + <instance> (only <template> in case of JMS) from the file. 
     * Provides meta-data for updating the config params.
     * 
     * @param extSysId External System Id.
     * 
     * @param mapFileProps Map of CAPSProperty objects which contain property values 
     * from the file. 
     * 
     * @param encryption Encryption scheme to be used. Template params which 
     * have attribute 'isEncrypted=true', but have clear-text values in the file
     * will be encrypted. Corresponding values will be updated in mapFileProps.
     * 
     * @return Base64 or B64-GZ (compressed then Base64 encoded) Configuration
     * String with updated param values from the file. Only if extSysId represents
     * eInsight Engine, LDAP or a JCA, the returned value is Base64 encoded. 
     * Otherwise value returned is B64-GZ encoded (this will be the case for all
     * eWay Configurations).
     * 
     * @throws java.lang.Exception
     */
    private String update( ESConfig esc, String extSysId, 
            Map<String, CAPSProperty> mapFileProps, String encryption) throws Exception {

        Map<String, Node> tparams = esc.getTemplateParamNodes();
        Map<String, Node> iparams = esc.getInstanceParamNodes();

        update(extSysId, tparams, iparams, mapFileProps, encryption);

        Document docConfig = esc.getConfigDOM();
        String xml = ConfigUtil.DOMToString(docConfig);
        String encoded = "";
        
        if( extSysId.endsWith(Constants.EINSIGHT_ENGINE) 
                || extSysId.endsWith(Constants.LDAP)
                || extSysId.startsWith(Constants.JCA_PREFIX)){
            // import Base64 (B64) for eInsight and LDAP
            encoded = ConfigUtil.encodeB64(xml);
        } else {
            // import compressed (B64-GZ) for eWays and JMS
            encoded = ConfigUtil.encodeB64GZ(xml);
        }
                
        return encoded;
    }

    private void update(String extSysId, Map<String, Node> tParams, 
            Map<String, Node> iParams, Map<String, CAPSProperty> mapFileProps, 
            String encryption) throws Exception {

        boolean useLongKeys = ConfigUtil.checkNameConflicts(tParams);
        boolean updatePropertyFile = false;

        Set<String> tparamKeys = tParams.keySet();
        for (Iterator<String> it = tparamKeys.iterator(); it.hasNext();) {

            String paramKey = it.next();
            Node tparam = tParams.get(paramKey);
            Node iparam = null;
            // iParams would be null for JMS
            if (iParams != null) {
                iparam = iParams.get(paramKey);
            }

            NamedNodeMap tAttribs = tparam.getAttributes();
            if (!ConfigUtil.isEditable(tAttribs)) {
                continue;
            }

            String name = tAttribs.getNamedItem("name").getTextContent();
            String key = name;
            if (useLongKeys) {
                key = paramKey;
            }

            // isEncrypted
            Node encrypAttrib = tAttribs.getNamedItem("isEncrypted");
            boolean isEncrypted = false;
            if (encrypAttrib != null) {
                isEncrypted = Boolean.parseBoolean(encrypAttrib.getTextContent());
            }

            // get value in property file
            CAPSProperty cprop = mapFileProps.get(key);
            if (cprop == null) {
                // delete the param if property file has no entry for it.
                deleteParam(iparam, tparam);
                continue;
            }
            
            String filevalue = cprop.getValue();
            String newvalue = filevalue;
            if (isEncrypted) {
                boolean isLocalValueEnc = newvalue.startsWith(encryption);
                if (isLocalValueEnc) {
                    newvalue = newvalue.substring(encryption.length());
                } else {
                    
                    String prefixednewvalue = newvalue;

                    // encrypt the clear-text value. This is the value that 
                    // would be imported.
                    if (Constants.ENC07.equals(encryption)) {
                        // use eWays encryption scheme
                        newvalue = ScEncrypt.encrypt("", newvalue);
                        
                        // prefix the encrypted value for property file.
                        prefixednewvalue = Constants.ENC07 + newvalue;

                    } else if (Constants.ENC08.equals(encryption)) {
                        // use JMS encryption scheme
                        //newvalue = ConfigUtil.encodeB64(newvalue);
                        newvalue = Base64.encode(newvalue, Constants.UTF_8);
                        // eWays runtime does not use prefixed encrypted values.
                        // JMS runtime does.
                        newvalue = Constants.ENC08 + newvalue;
                        
                        // prefix the encrypted value for property file.
                        prefixednewvalue = newvalue;

                    }

                    // prefix the encrypted value for property file.
                    // String prefixednewvalue = Constants.ENC07 + newvalue;

                    // update the encrypted prefixed value in mapFileProps so it will 
                    // be written to file
                    cprop.setValue(prefixednewvalue);

                    updatePropertyFile = true;
                }
            }

            if (iparam != null) {
                // update instance param value if the instance param exists.
                // Note that JMS does not have the <instance> section.
                Node valueNode = (Node) xp.evaluate("value", iparam, XPathConstants.NODE);
                if (valueNode == null) {
                    valueNode = createValueChild(iparam);
                }

                newvalue = Constants.CDATA_PREFIX + newvalue + Constants.CDATA_SUFFIX;
                valueNode.setTextContent(newvalue);
            }
        }

        if (updatePropertyFile) {
            Collection<CAPSProperty> updatedprops = mapFileProps.values();
            List<CAPSProperty> lupdatedprops = new ArrayList<CAPSProperty>(updatedprops);
            mapFilesToUpdate.put(extSysId, lupdatedprops);
        }

    }

    private Node createValueChild(Node iparam) {
        Document doc = iparam.getOwnerDocument();
        Node valueNode = doc.createElement("value");
        CDATASection cdata = doc.createCDATASection("");
        valueNode.appendChild(cdata);
        iparam.appendChild(valueNode);
        return valueNode;
    }

    private void deleteParam(Node iparam, Node tparam) {
        // tparam should never be null
        // iparam can be null (for JMS)
        if (iparam != null) {
            Node isection = iparam.getParentNode();
            isection.removeChild(iparam);
        }

        Node tsection = tparam.getParentNode();
        tsection.removeChild(tparam);
    }
    
    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }
}
