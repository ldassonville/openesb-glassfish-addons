/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ObjFactory.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import org.glassfish.openesb.addons.appconfig.ConfigUtil;
import org.glassfish.openesb.addons.appconfig.Constants;
import java.io.ByteArrayInputStream;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public class ObjFactory implements ObjectFactory {

    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment) throws Exception {

        Properties ret = null;
        if (obj instanceof Reference) {
            ret = new Properties();
            Reference r = (Reference) obj;
            for (Enumeration<RefAddr> all = r.getAll(); all.hasMoreElements();) {
                RefAddr ra = all.nextElement();
                String type = ra.getType();
                if (ra.getType().equals("Options")) {
                    String content = (String) ra.getContent();
                    if(content.startsWith(Constants.B64_GZ_PREFIX)){
//                        System.out.println("Options: " + content);
                        content = ConfigUtil.decodeB64GZ(content);
                        content = ConfigUtil.unpackOptions(content);
//
//                        System.out.println("Options: decoded: " + content);
//                        java.util.Properties prop = new java.util.Properties();
//                        prop.load(new ByteArrayInputStream(content.getBytes("UTF-8")));
//                        System.out.println("props: line2: " + prop.getProperty("line2"));

                    }
                    ret.put(ra.getType(), content);
                } else {
                    ret.put(ra.getType(), ra.getContent());
                }
            }
        }
        return ret;
    }
}

