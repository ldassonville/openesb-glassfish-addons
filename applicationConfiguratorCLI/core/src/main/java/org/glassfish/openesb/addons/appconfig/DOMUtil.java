/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DOMUtil.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import java.io.File;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.InputStream;
import java.io.StringWriter;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * @author Sun Microsystems
 * 
 */
public class DOMUtil {
    
    protected final static String MODULE_JAR = Constants.MODULE_JAR;
    protected final static String MODULE_RAR = Constants.MODULE_RAR;
    protected final static String MODULE_WAR = Constants.MODULE_WAR;

    public static String writeToString(Node node) throws Exception {
        // Prepare the DOM document for writing
        Source source = new DOMSource(node);

        StringWriter sw = new StringWriter();
        Result result = new StreamResult(sw);

        // Write the DOM document to the file
        Transformer xformer = TransformerFactory.newInstance().newTransformer();
        xformer.transform(source, result);
        
        return sw.toString();
    }
    
    public static Document parseXmlFile(File f) {
        Document doc = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            if (f.exists()) {
                doc = db.parse(f);
            } else {
                throw new RuntimeException("file does not exist: "
                        + f.getAbsolutePath());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doc;
    }

    public static Document parseXmlFile(String filePath) {
        return parseXmlFile(new File(filePath));
    }
 
    public static Document parse(InputStream is) {
        Document doc = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            if (is != null ) {
                doc = db.parse(is);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return doc;
    }
     
    public static Document parse(String stringSource) {
        Document doc = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            doc = db.parse(new InputSource(new StringReader(stringSource)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doc;
    }

    public static Document createDocument() {
        Document doc = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.newDocument();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return doc;
    }

    public static String getRAClass(Document desc) {
        String raClass = "";
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            Node nodeRAClass = (Node) xpath.evaluate(
                    "/connector/resourceadapter/resourceadapter-class", desc,
                    XPathConstants.NODE);
            raClass = nodeRAClass.getTextContent();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return raClass;
    }

    public static String getEJBName(Node ejbNode) {
        String ejbName = "";
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            Node nodeEJBName = (Node) xpath.evaluate("//ejb-name", ejbNode,
                    XPathConstants.NODE);
            ejbName = nodeEJBName.getTextContent();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ejbName;
    }

    public static String getEJBClass(Node ejbNode) {
        String ejbClass = "";
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            Node nodeEJBClass = (Node) xpath.evaluate("//ejb-class", ejbNode,
                    XPathConstants.NODE);
            ejbClass = nodeEJBClass.getTextContent();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ejbClass;
    }

    public static Node getConfigurationEJBNode(Document desc) {
        Node ejbNode = null;
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            Node nodeEJBs = (Node) xpath.evaluate("/ejb-jar/enterprise-beans",
                    desc, XPathConstants.NODE);
            NodeList nlEJBs = nodeEJBs.getChildNodes();
            int n = nlEJBs.getLength();
            String configuration = null;
            for (int i = 0; i < n; i++) {
                Node node = nlEJBs.item(i);

                if (node.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }

                String nodeName = node.getNodeName();
                //print("getConfigurationEJBNode: nodeName: " + nodeName);
                if (!("session".equals(nodeName) || "message-driven"
                        .equals(nodeName))) {
                    continue;
                }

                String cfgXpath = "activation-config/activation-config-property/activation-config-property-value[../activation-config-property-name='"
                        + Constants.Configuration + "']";

                Node nodePropValue = (Node) xpath.evaluate(cfgXpath, node,
                        XPathConstants.NODE);

                if (nodePropValue != null) {
                    //print("getConfigurationEJBNode: nodeValue not null");
                    configuration = nodePropValue.getTextContent();
                }

                if (configuration != null && configuration.trim().length() > 0) {
                    //print("getConfigurationEJBNode: valid configuration value");
                    ejbNode = node;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ejbNode;
    }

    public static String getPropertyValue(String propName, Document modDesc, String modType){
        String propValue = null;
        try{
            String propXpath = "";
            if (MODULE_JAR.equals(modType)) {
                propXpath = "//activation-config-property-value[../activation-config-property-name='"
                        + propName + "']";
            } else if (MODULE_RAR.equals(modType)) {
                propXpath = "//config-property-value[../config-property-name='"
                        + propName + "']";
            } else if (MODULE_WAR.equals(modType)) {
                return null;
            } else {
                return null;
            }

            XPath xpath = XPathFactory.newInstance().newXPath();
            Node nodePropValue = (Node) xpath.evaluate(propXpath, modDesc,
                    XPathConstants.NODE);

            if (nodePropValue != null) {
                propValue = nodePropValue.getTextContent();
            }
            
        }catch (XPathExpressionException xpe) {
            xpe.printStackTrace();
        }
        return propValue;
    }

    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }
}
