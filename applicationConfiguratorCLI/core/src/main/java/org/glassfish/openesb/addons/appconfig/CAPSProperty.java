/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CAPSProperty.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import java.util.List;


/**
 * @author Sun Microsystems
 *
 */
public class CAPSProperty {
    public static final String COMMENT_DESC_PREFIX    = "# ";
    public static final String COMMENT_VALID_OPTIONS = COMMENT_DESC_PREFIX + "Valid Options (copy values inside [ ]):";
    public static final String COMMENT_OPTION_PREFIX    = "#* ";
    public static final String SPACE    = " ";
    
    private String mKey = null;
    private String mDisplayName = null;
    private String mValue = null;
    private String mDescription = null;
    private List<String> mChoiceList = null;
    
    public CAPSProperty() {
        // TODO Auto-generated constructor stub
    }

    public CAPSProperty(String key, String displayName, String value, String description, List<String> choiceList) {
        mKey = key;
        mDisplayName = displayName;
        mValue = value;
        mDescription = description;
        mChoiceList = choiceList;
    }
  
    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        mKey = key;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public void setDisplayName(String displayName) {
        mDisplayName = displayName;
    }

    public List<String> getChoiceList() {
        return mChoiceList;
    }

    public void setChoiceList(List<String> choiceList) {
        mChoiceList = choiceList;
    }

}