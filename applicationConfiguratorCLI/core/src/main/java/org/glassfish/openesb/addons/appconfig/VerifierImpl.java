/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)VerifierImpl.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.management.MBeanServerConnection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 *
 * @author Sun Microsystems
 */
public class VerifierImpl implements Verifier {

    MBeanServerConnection mbsc;
    JNDIHandler jndiHdlr;
    
    public VerifierImpl(){
        jndiHdlr = new JNDIHandler();
    }
    
    public VerifierImpl(MBeanServerConnection conn){
        mbsc = conn;
        if(mbsc != null){
            jndiHdlr = new JNDIHandler(mbsc);
        }else{
            jndiHdlr = new JNDIHandler();
        }
    }
    
    public List<VerificationResult> verify(String strDoc) throws Exception {

        List<VerificationResult> vrList = new ArrayList<VerificationResult>();
        Document modDesc = DOMUtil.parse(strDoc);
        Element e = modDesc.getDocumentElement();
        String root = e.getTagName();
        String modType = "";
        if ("ejb-jar".equals(root)) {
            modType = Constants.MODULE_JAR;
        } else if ("connector".equals(root)) {
            modType = Constants.MODULE_RAR;
        } else {
            return vrList;
        }

        // check if its a valid descriptor for verification
        if (!ConfigUtil.validModule("", modType, modDesc)) {
            return vrList;
        }

        String projectInfo = DOMUtil.getPropertyValue("ProjectInfo", modDesc, modType);
        Document projectInfoDoc = null;
        if (projectInfo != null) {
            projectInfo = ConfigUtil.decode(projectInfo);
            projectInfoDoc = DOMUtil.parse(projectInfo);
        }

        String envId = ConfigUtil.getEnvId(modDesc, projectInfoDoc, modType);
        String cmId = ConfigUtil.getCMId(projectInfoDoc);

        if (envId == null && cmId == null) {
            return vrList;
        }

        VerificationResult vrEnv = doVerify(envId, modType, modDesc);
        VerificationResult vrCM = doVerify(cmId, modType, modDesc);

        if (vrEnv != null) {
            vrList.add(vrEnv);
        }

        if (vrCM != null) {
            vrList.add(vrCM);
        }

        return vrList;
    }

    private VerificationResult doVerify(String extSysId, String modType, Document modDesc) throws IOException {

        VerificationResult vr = new VerificationResult();
      
        String jndiName = JNDIHandler.getJNDIName(Constants.CAPSENV_ROOT, extSysId);

        String cmPrefix = Constants.CAPSENV_ROOT +
                Constants.JNDI_PATH_SEPARATOR + Constants.CM;

        if( jndiName.startsWith(cmPrefix) ) {

            // eInsight and JMS do not have CM overrides. 
            // Return NA status for these.
            
            if(jndiName.endsWith(Constants.EINSIGHT_ENGINE)){
                vr.setStatus(VerificationResult.NA);
                return vr;
            }
            
            if (Constants.MODULE_RAR.equals(modType)) {
                String raClass = DOMUtil.getRAClass(modDesc);
                if ( raClass.startsWith("com.stc.jmsjca.") ) {
                    // JMS does not have CM property Overrides so do not check for them.
                    vr.setStatus(VerificationResult.NA);
                    return vr;
                }
            }
        }
        
        // check if the required override exists in server JNDI
        if (!jndiHdlr.existsCR(jndiName)) {
            vr.setStatus(VerificationResult.WARNING);
        } else {
            vr.setStatus(VerificationResult.OK);
        }

        vr.setJndiName(jndiName);
        vr.setJndiClassType("java.lang.String");

        if (Constants.MODULE_RAR.equals(modType)) {
            String raClass = DOMUtil.getRAClass(modDesc);
            vr.setReferencedBy("Resource Adapter");
            vr.setReferencedClass(raClass);
        } else if (Constants.MODULE_JAR.equals(modType)) {
            Node ejbNode = DOMUtil.getConfigurationEJBNode(modDesc);
            String ejbName = DOMUtil.getEJBName(ejbNode);
            String ejbClass = DOMUtil.getEJBClass(ejbNode);

            vr.setReferencedBy(ejbName);
            vr.setReferencedClass(ejbClass);
        }

        return vr;
    }

}
