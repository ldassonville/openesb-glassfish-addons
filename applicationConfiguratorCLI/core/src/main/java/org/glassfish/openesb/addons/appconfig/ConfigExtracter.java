/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigExtracter.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import org.w3c.dom.*;
import java.util.*;
import java.util.ArrayList;
import javax.xml.xpath.*;

/**
 *
 * @author Sun Microsystems
 */
public class ConfigExtracter {

    //private Ear ear;
    private String configdir;
    private XPathFactory xpf = XPathFactory.newInstance();
    private XPath xp = xpf.newXPath();

    public ConfigExtracter(String configDir) {
        configdir = configDir;
    }
    
    /*
     * Extracts (to property files) all external system configuration parameters 
     * from the given CAPS application EAR file.
     */
    public void extract(String earPath, boolean overwrite) 
            throws Exception {
        
        Map<String, List<CAPSProperty>> map = getEarConfigProperties(earPath);
        writePropertyFiles(map, overwrite);
    }

    
    /*
     * Extracts (to property files) configuration parameters for the given 
     * external system (extSysId), from the given encoded CAPS Configuration 
     * string (config).
     */
    public void extract(String extSysId, String config, boolean overwrite)
            throws Exception {
        
        Map<String, List<CAPSProperty>> map = new HashMap<String, List<CAPSProperty>>();
        String configXml = ConfigUtil.decode(config);
        //List<CAPSProperty> props = getConfigProperties(extSysId, configXml);
        List<CAPSProperty> props = getEnvProperties(extSysId, configXml, null, null);
        map.put(extSysId, props);
        writePropertyFiles(map, overwrite);
        
    }

    /*
     * Extracts (to property files) configuration parameters for the given 
     * external system (extSysId), from the given encoded CAPS Configuration 
     * string (config).
     */
    public void extract(String extSysId, String config, Document modDesc, boolean overwrite)
            throws Exception {
        
        Map<String, List<CAPSProperty>> map = new HashMap<String, List<CAPSProperty>>();
        String configXml = ConfigUtil.decode(config);
        //List<CAPSProperty> props = getConfigProperties(extSysId, configXml);
        List<CAPSProperty> props = getEnvProperties(extSysId, configXml, null, modDesc);
        map.put(extSysId, props);
        writePropertyFiles(map, overwrite);
        
    }

    private void writePropertyFiles(Map<String, List<CAPSProperty>> map, 
            boolean overwrite) throws Exception {

        if (map.isEmpty()) {
            return;
        }

        ConfigDirHandler cdh = new ConfigDirHandler(configdir);
        cdh.write(map, overwrite);
    }

    /**
     * Get all Java CAPS External System configuration properties from the 
     * specified EAR file.
     *
     * @param earPath Path to the Java CAPS EAR application file. 
     * 
     * @return A Map<String ext-sys-id, List<CAPSProperty>>, where ext-sys-id
     * is derived from the application's CAPS Environment (ENV) and 
     * Connectivity Map (CM) object names and can be one of following forms: 
     *      "ENV:<EnvironmentName>:<LogicalHostName>:<ExternalSystemName>" 
     *      "ENV:<EnvironmentName>:<LogicalHostName>:<ExternalSystemName>_Inbound" 
     *      "ENV:<EnvironmentName>:<LogicalHostName>:<ExternalSystemName>_Outbound" 
     *      "CM:<ProjectName>:<CollabName>:<ComponentName>" 
     * 
     * @throws Exception If any errors occur.
     */
    
    private Map<String, List<CAPSProperty>> getEarConfigProperties(String earPath) throws Exception {

        Ear ear = new Ear(earPath);

        Map<String, List<CAPSProperty>> map = new HashMap<String, List<CAPSProperty>>();

        List<Node> listAppModules = ear.listModules();
        for (Iterator<Node> it = listAppModules.iterator(); it.hasNext();) {
            Node moduleNode = it.next();

            String modName = moduleNode.getTextContent();
            String modType = ConfigUtil.getModuleType(modName);

            Document modDesc = ear.getDescriptorAsDOM(modName);
            
            //check if its a valid module for Configuration processing
            if (!ConfigUtil.validModule(modName, modType, modDesc)) {
                continue;
            }
            
            String projectInfo = DOMUtil.getPropertyValue("ProjectInfo", modDesc, modType);
            Document projectInfoDoc = null;
            if (projectInfo != null) {
                projectInfo = ConfigUtil.decode(projectInfo);
                projectInfoDoc = DOMUtil.parse(projectInfo);
            }

            String envId = ConfigUtil.getEnvId(modDesc, projectInfoDoc, modType);
            String cmId = ConfigUtil.getCMId(projectInfoDoc);
            
            if (envId == null && cmId == null) {
                continue;
            }

            String strCmapParams = ConfigUtil.getCMapParamsFromProjectInfo(projectInfoDoc);

//            print("processing module:" + modName);

            String config = ConfigUtil.getDescriptorConfig(modType, modDesc);
            String configXml = ConfigUtil.decode(config);

            //List<CAPSProperty> props = getConfigProperties(envId, cmId, config, strCmapParams, modDesc);
            List<CAPSProperty> propsEnv = getEnvProperties(envId, configXml, strCmapParams, modDesc);
            if (propsEnv != null && propsEnv.size() > 2 ) {
                // add to map only if the list contains properties
                // other than extsys.id and Configuration
                map.put(envId, propsEnv);
            }
            
            List<CAPSProperty> propsCM = getCMProperties(cmId, configXml, strCmapParams, modDesc);
            if (propsCM != null && propsCM.size() > 2 ) {
                // add to map only if the list contains properties
                // other than extsys.id and Configuration
                map.put(cmId, propsCM);
            }
        }

        return map;

    }

    private List<CAPSProperty> getIdAndConfigurationProps(String extId, String configuration) throws Exception {

        List<CAPSProperty> props = new ArrayList<CAPSProperty>();
        // add external system id
        CAPSProperty propExtSysId = new CAPSProperty(Constants.ExtSysId_KEY,
                Constants.ExtSysId_DISPLAY_NAME, extId,
                Constants.ExtSysId_DESC, null);
        props.add(propExtSysId);

        String b64gz = ConfigUtil.encodeB64GZ(configuration);
        CAPSProperty propConfig = new CAPSProperty(Constants.Configuration,
                "", b64gz, Constants.Configuration_DESC, null);

        props.add(propConfig);
        return props;
    }

    private List<CAPSProperty> getEnvProperties(String envId, String configXml, 
            String strCmapParams, Document modDesc) throws Exception {
        List<CAPSProperty> props = new ArrayList<CAPSProperty>();
        List<CAPSProperty> idAndConfig = getIdAndConfigurationProps(envId, configXml);
        props.addAll(idAndConfig);

        List<CAPSProperty> envParamProps = getEnvProperties(configXml, strCmapParams, modDesc);
        props.addAll(envParamProps);
        return props;
    }
      
    private List<CAPSProperty> getEnvProperties(String configXml, String strCmapParams, Document modDesc) throws Exception {
        List<CAPSProperty> props = new ArrayList<CAPSProperty>();
        ESConfig esc = new ESConfig(configXml, strCmapParams, modDesc);
        List paramProps = esc.getEnvParamProperties();
        props.addAll(paramProps);
        return props;
    }
    
    private List<CAPSProperty> getCMProperties(String cmId, String configXml, String strCmapParams, Document modDesc) throws Exception {
        List<CAPSProperty> props = new ArrayList<CAPSProperty>();
        List<CAPSProperty> idAndConfig = getIdAndConfigurationProps(cmId, configXml);
        props.addAll(idAndConfig);

        List<CAPSProperty> cmParamProps = getCMProperties(configXml, strCmapParams, modDesc);
        props.addAll(cmParamProps);
        return props;
    }

    private List<CAPSProperty> getCMProperties(String configuration, String strCmapParams, Document modDesc) throws Exception {
        List<CAPSProperty> props = new ArrayList<CAPSProperty>();
        ESConfig esc = new ESConfig(configuration, strCmapParams, modDesc);
        List paramProps = esc.getCMParamProperties();
        props.addAll(paramProps);
        return props;
    }

    
    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }
}
