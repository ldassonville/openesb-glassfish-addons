/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)VerificationResult.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

/**
 * @author Sun Microsystems
 *
 */
public class VerificationResult {

    public static final int NA = 0;
    public static final int OK = 1;
    public static final int WARNING = 2;
    //public static int ERROR = 3;

    private String referencedBy;
    private String referencedClass;
    private String jndiName;
    private String jndiClassType;
    private String description;
    private int status = 0;
    
    public String getReferencedBy() {
        return referencedBy;
    }
    public void setReferencedBy(String referencedBy) {
        this.referencedBy = referencedBy;
    }
    public String getReferencedClass() {
        return referencedClass;
    }
    public void setReferencedClass(String referencedClass) {
        this.referencedClass = referencedClass;
    }
    public String getJndiName() {
        return jndiName;
    }
    public void setJndiName(String jndiName) {
        this.jndiName = jndiName;
    }
    public String getJndiClassType() {
        return jndiClassType;
    }
    public void setJndiClassType(String jndiClassType) {
        this.jndiClassType = jndiClassType;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    
}
