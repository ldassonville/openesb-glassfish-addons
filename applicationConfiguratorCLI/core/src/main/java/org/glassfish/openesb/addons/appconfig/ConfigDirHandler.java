/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigDirHandler.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
package org.glassfish.openesb.addons.appconfig;

import com.sun.enterprise.cli.framework.CLILogger;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.Map.*;

import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Set;

public class ConfigDirHandler {

    private static final String PROPERTIES_SUFFIX = ".properties";
    public static final String COMMENT_DESC_PREFIX = "# ";
    public static final String COMMENT_OPTION_PREFIX = "#* ";
    public static final String OPTION_BEGIN_MARKER = "[";
    public static final String OPTION_END_MARKER = "]";
    public static final String COMMENT_VALID_OPTIONS = COMMENT_DESC_PREFIX + "Valid Options (copy values inside " + OPTION_BEGIN_MARKER + " " + OPTION_END_MARKER + "):";
    public static final String SPACE = " ";
    private String mConfigDir = null;

    // private Map<String, Properties> mapProperties = null;
    private Map<String, Map<String, CAPSProperty>> mapProperties = null;

    public ConfigDirHandler() {
    }

    public ConfigDirHandler(String configDir) {
        this.mConfigDir = configDir;
        this.mapProperties = new HashMap<String, Map<String, CAPSProperty>>();
    }

//    public String extSysIdToPath(String extSysId) {
//        return ConfigUtil.idToPath(extSysId, File.separator);
//    }

    public Map<String, String> getAllConfigurations() throws Exception {
        Map<String, String> map = new HashMap<String, String>();
        File dir = new File(mConfigDir);
        loadESConfigurations(dir, map);
        return map;
    }

    private void loadESConfigurations(File dir, Map<String, String> map) throws Exception {

        File[] files = dir.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            if (file.isDirectory()) {
                loadESConfigurations(file, map);
                continue;
            }

            String fName = file.getName();
            if (!fName.endsWith(".properties")) {
                continue;
            }

            Properties props = new Properties();
            props.load(new FileInputStream(file));
            String extSysId = props.getProperty(Constants.ExtSysId_KEY);
            String configuration = props.getProperty(Constants.Configuration);
            configuration = ConfigUtil.decodeAndCleanB64GZ(configuration);
            map.put(extSysId, configuration);
        }

    }

    public Map<String, CAPSProperty> getCAPSProperties(String extSysId)
            throws Exception {
        String propFilePath = getPropertiesFilePath(extSysId);
        File propFile = new File(propFilePath);
        return loadProperties(propFile);
    }

    public String getPropertiesFilePath(String extSysId) {
        //return mConfigDir + File.separator + extSysIdToPath(extSysId) + PROPERTIES_SUFFIX;
        
        // clean sub-project names
        extSysId = extSysId.replace(" | ", Constants.FILE_PATH_SEPARATOR);
        //extSysId = extSysId.replace("|", Constants.FILE_PATH_SEPARATOR);
        return mConfigDir + Constants.FILE_PATH_SEPARATOR + 
                extSysId.replace(Constants.ID_SEPARATOR, 
                Constants.FILE_PATH_SEPARATOR) + PROPERTIES_SUFFIX;
    }

    public void write(Map<String, List<CAPSProperty>> map, boolean overwrite) throws Exception {

        if (!overwrite && existsAny(map) ){
            throw new Exception("One or more files exist. Aborting write. " +
                    "To force overwrite use '--force=true' option.");
        }
        
        Set<String> extSysIds = map.keySet();
        for (Iterator<String> it = extSysIds.iterator(); it.hasNext();) {
            String extSysId = it.next();
            List<CAPSProperty> props = map.get(extSysId);
            write(extSysId, props);
        }
    }

    private boolean existsAny(Map<String, List<CAPSProperty>> map){
        Set<String> extSysIds = map.keySet();
        for (Iterator<String> it = extSysIds.iterator(); it.hasNext();) {
            String extSysId = it.next();
            String path = getPropertiesFilePath(extSysId);
            File f = new File(path);
            if(f.exists()){
                return true;
            }
        }
        return false;
    }
    
    public void write(String extSysId, List<CAPSProperty> props) throws IOException {

        String propFilePath = getPropertiesFilePath(extSysId);

        File propFile = new File(propFilePath);

        if (!propFile.getParentFile().exists()) {
            propFile.getParentFile().mkdirs();
        }

        FileWriter fWriter = new FileWriter(propFile);
        PrintWriter out = new PrintWriter(new BufferedWriter(fWriter));

        for (Iterator<CAPSProperty> it = props.iterator(); it.hasNext();) {

            CAPSProperty cProp = it.next();

            /*
             * Write in following format
             *  # display name 
             *  # description 
             *  # Valid Options: #* Option1 
             *  #* Option2 
             *  #* Option3 
             *  key=value
             * 
             */

            String displayName = cProp.getDisplayName();
            if (displayName != null) {
                out.println(COMMENT_DESC_PREFIX + displayName);
            }

            // description
            String desc = cProp.getDescription();
            if (desc != null && desc.trim().length() > 0) {
                out.println(wordWrap(desc, 80));
            }

            List<String> choiceList = cProp.getChoiceList();
            if (choiceList != null && choiceList.size() > 0) {
                out.println(COMMENT_VALID_OPTIONS);
                for (String option : choiceList) {
                    out.println(COMMENT_OPTION_PREFIX + OPTION_BEGIN_MARKER + option + OPTION_END_MARKER);
                }
            }

            if(cProp.getKey().equals("Options")){
                //print("Writing Options: " + cProp.getValue());
                cProp.setKey("::Options");
                String val = Constants.LINE_FEED 
                        + cProp.getValue() + Constants.LINE_FEED 
                        + "::Options::";
                cProp.setValue(val);
            }
            
            // key and (cleaned) value
            out.println(cProp.getKey() + "=" + cleanValue(cProp.getValue()));
            out.println();
        }

        out.flush();

        try {
            out.close();
        } catch (Exception e) {
        }

    }

    /**
     * Load Java CAPS Configuration Property files. 
     * 
     * @param propFile File object to load properties from.
     * 
     * @return Map<key,CAPSProperty>
     *  
     */
    public Map<String, CAPSProperty> loadProperties(File propFile)
            throws IOException, Exception {

        BufferedReader rdr = null;
        Map<String, CAPSProperty> mapProps = new LinkedHashMap<String, CAPSProperty>();

        rdr = new BufferedReader(new FileReader(propFile));
        String str = null;
        StringBuffer buf = new StringBuffer();
        String key = "";
        String displayName = "";
        String value = "";
        List<String> choiceList = new ArrayList<String>();
        boolean propStart = true;

        while ((str = rdr.readLine()) != null) {

            if (str.trim().length() == 0) {
                continue;
            }

            /*
             * Read in following format
             *  # display name 
             *  # description 
             *  # Valid Options: 
             *  #* Option1 
             *  #* Option2 
             *  #* Option3 
             *  key=value
             * 
             */

            if (str.startsWith(COMMENT_DESC_PREFIX)) {
                if (propStart) {
                    // display name
                    displayName = str.substring(COMMENT_DESC_PREFIX.length());
                    propStart = false;
                    continue;
                }
                if (str.startsWith(COMMENT_VALID_OPTIONS)) {
                    // ignore this comment and process next lines
                    continue;
                }

                // description
                if (buf.toString().length() > 0) {
                    buf.append(" " + str.substring(COMMENT_DESC_PREFIX.length()));
                } else {
                    buf.append(str.substring(COMMENT_DESC_PREFIX.length()));
                }
                continue;

            } else if (str.startsWith(COMMENT_OPTION_PREFIX)) {
                String option = str.substring(COMMENT_OPTION_PREFIX.length()).trim();
                if (option.startsWith(OPTION_BEGIN_MARKER)) {
                    option = option.substring(OPTION_BEGIN_MARKER.length(),
                            option.length() - OPTION_END_MARKER.length());
                }

                choiceList.add(option);
                continue;
            }

            int i = str.indexOf("=");
            key = str.substring(0, i);
            if(key.startsWith("::Options")){
                key = key.substring("::".length());
            }
            value = str.substring(i + 1);

            if(key.equals("Options")){
                str = rdr.readLine();
                while(str!= null && !str.equals("::Options::")){
                    value = value.length() == 0 ? str : value + Constants.LINE_FEED + str;
                    str = rdr.readLine();
                }
            }
            
            // keep the value as-is. Do not clean.
            // boolean cleanValue = false;
            CAPSProperty cProp = new CAPSProperty(key.trim(), displayName.trim(), value, buf.toString(), choiceList);
            mapProps.put(key, cProp);

            // start next property in next iteration
            propStart = true;

            // re-initialize the buffer, choicelist
            buf = new StringBuffer();
            choiceList = new ArrayList<String>();
        }

        if (rdr != null) {
            try {
                rdr.close();
            } catch (Exception e) {
            }
        }

        return mapProps;

    }
    
    /*
     * escape special characters in the given string
     */
    private String cleanValue(String str) {

        if (str == null || str.length() == 0) {
            return str;
        }

        // do not attempt to clean encoded strings
        if (str.startsWith(Constants.B64_PREFIX)) {
            return str;

        }

        int i = str.indexOf('\\');
        if ((i < 0)) {
            return str;
        }

        if (i + 1 == str.length()) {
            return str.replaceAll("\\\\", "\\\\\\\\");
        }

        if (str.charAt(i + 1) == '\\') {
            // TODO: handle this better
            // with regex. Check the entire
            // string rather than just the first
            // appreance of '\'.
            return str;
        }

        // escape all '\' characters.
        return str.replaceAll("\\\\", "\\\\\\\\");
    }

    /*
     * WordWrap: does not break words. Appends complete words if within width
     * limit, else ends line at previous word.
     */
    private String wordWrap(String str, int width) {
        String newline = System.getProperty("line.separator");

        // print("wordWrap: " + str);
        if (str == null) {
            return COMMENT_DESC_PREFIX;
        }
        if (str.length() < width) {
            return COMMENT_DESC_PREFIX + str;
        }

        // StringTokenizer st = new StringTokenizer(str, " ", false);
        StringTokenizer st = new StringTokenizer(str);
        ArrayList<String> words = new ArrayList<String>();
        while (st.hasMoreElements()) {
            String word = st.nextToken();
            words.add(word);
        }

        StringBuffer strBuf = new StringBuffer();
        boolean newLine = true;
        int curLen = 0;
        Object[] wordsArr = words.toArray();
        String word = "";
        for (int i = 0; i < wordsArr.length; i++) {
            word = (String) wordsArr[i];
            if (curLen + word.length() < width) {
                if (newLine) {
                    if (!word.startsWith(COMMENT_DESC_PREFIX)) {
                        strBuf.append(COMMENT_DESC_PREFIX + word);
                        curLen += word.length() + COMMENT_DESC_PREFIX.length();
                    } else {
                        strBuf.append(word);
                        curLen += word.length();
                    }
                    newLine = false;
                } else {
                    strBuf.append(SPACE + word);
                    curLen += word.length() + SPACE.length();
                }
            } else {
                if (word.length() >= width) {
                    strBuf.append(newline + COMMENT_DESC_PREFIX + word + newline);
                    newLine = true;
                    curLen = 0;
                    continue;
                }
                // keep the current word for next iteration
                i--;
                strBuf.append(newline);
                newLine = true;
                curLen = 0;
            }
        }

        return strBuf.toString();
    }

    /*
     * WordWrap: break words at width boundary with "-"
     */
    private String wordWrap2(String str, int width) {
        if (str == null || str.length() < width) {
            return "#" + str;
        }

        int len = str.length();
        StringBuffer strBuf = new StringBuffer();
        String contdPrefix = "";
        for (int i = 0; i < len; i += width) {
            // remove all leading whitespaces.
            while (true) {
                if (Character.isWhitespace(str.charAt(i))) {
                    i++;
                } else {
                    break;
                }
            }

            String subStr = "";
            if (i + width > len) {
                subStr = str.substring(i);
                strBuf.append("#" + contdPrefix + subStr);
                break;
            }

            if (str.length() > i + width) {
                char nextChar = str.charAt(i + width + 1);
                if (Character.isWhitespace(nextChar) || nextChar == '.') {
                    strBuf.append("#" + contdPrefix + str.substring(i, i + width) + "\n");
                } else {
                    strBuf.append("#" + contdPrefix + str.substring(i, i + width) + "-" + "\n");
                    contdPrefix = "-";
                }
            }
        }
        return strBuf.toString();
    }

    private static void print(String msg) {
        CLILogger.getInstance().printMessage(msg);
    }
}
